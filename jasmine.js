const 
    Jasmine = require('jasmine'),
    jasmine = new Jasmine(),
    JasmineConsoleReporter = require('jasmine-console-reporter');

jasmine.loadConfigFile('spec/support/jasmine.json');

const reporter = new JasmineConsoleReporter({
    colors: 1,
    cleanStack: 1,
    verbosity: 4,
    listStyle: 'indent',
    timeUnit: 'ms',
    timeThreshold: { ok: 500, warn: 1000, ouch: 3000 },
    activity: false,
    emoji: true,
    beep: true
});

jasmine.env.clearReporters();
jasmine.addReporter( reporter );
jasmine.execute();