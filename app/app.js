const 
    main = require('./lib'),
    server = require('./server');

console.log( main() );
const activeServer = server.listen(3000, () => { console.log('Listening port 3000 ...') });

let ticker = 0;
const interval = setInterval(() => {
    ticker ++;
    console.log( `Tick ${ticker}` );
    if(ticker >= 10) {
        activeServer.close();
        clearInterval( interval );
        console.log('...end app');
    }
}, 1000);