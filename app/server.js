const 
    itemService = require('./item.service');
    express = require('express'),
    app = express(),

app.get("/", function(req, res) {

    res.send("Hello world");

});

app.get("/:id", function(req, res) {

    const item = itemService.find( req.params.id );
    if( item ){
        res.setHeader('Content-Type', 'application/json');
        res.send( JSON.stringify(item) );
    }
    else {
        res.status(404).send('Not found');
    }

});

module.exports = app;